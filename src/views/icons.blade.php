<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ $meta['title'] or 'Error' }} | {{ config('app.name') }}</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        .img-margin {
            margin: 0 auto;
        }
    </style>

</head>

<body>

    <div class="container">

        @foreach($icons as $iconMaster=>$iconSet)
            <div class="row mt-5">
                <div class="col-md-12 mb-3">
                    <h3>{{ ucfirst($iconMaster) }}</h3>
                </div>
                <div class="clearfix"></div>

                @foreach($iconSet as $icon)
                    <div class="col-md-2">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3 text-center">
                            <div class="features-icons-icon d-flex mb-2">
                                <img class="img-fluid img-margin" src="{{ route('icon', ['iconName' => $icon, 'iconColor' => '000']) }}">
                            </div>
                            <p class="lead mb-0">{{ $icon }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>


<!-- Bootstrap core JavaScript -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>