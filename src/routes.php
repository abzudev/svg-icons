<?php

Route::get('/static/icons/{iconName}/{iconColor}.svg', ['uses' => '\SvgIcons\Icon@renderIcon', 'as'=>'icon']);
Route::get('/icons',['uses' => '\SvgIcons\Icon@svgIconList', 'as'=>'svgIconList']);