<?php namespace SvgIcons;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Icon
 * @package SVGICON
 */
class Icon{

    private $path = null;

    private $svgContent = null;

    private $colors = ['#000000'];

    public function __construct($path = null)
    {
        if(!is_null($path)) {
            $this->setPath($path);
        } else {
            $this->setPath(realpath(__DIR__ . '/../icons'));
        }
    }

    public function loadSvg($svgFile)
    {
        if(!file_exists($this->path.'/'.$svgFile['folder'].'/'.$svgFile['svg'])) {
            throw new \Exception($svgFile['folder'].'-'.$svgFile['svg'].' does not exist');
        }

        $this->svgContent = file_get_contents($this->path.'/'.$svgFile['folder'].'/'.$svgFile['svg']);
        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function getColors()
    {
        return $this->colors;
    }

    public function setColors($colors)
    {
        if(!is_array($colors)) {
            $colors = explode(',', $colors);
        }

        foreach ($colors as $colorIndex=>$color) {
            if(substr($color, 0, 1)!=='#') {
                $colors[$colorIndex] = '#'.$color;
            }
        }

        $this->colors = $colors;
        return $this;
    }

    public function getSvgContent()
    {
        return $this->svgContent;
    }

    public function processSvg($colors = null)
    {
        if(!is_null($colors) and is_array($colors)) {
            $this->setColor($colors);
        }

        $toReplace = [];
        foreach($this->getColors() as $colorIndex=>$color) {
            $toReplace[] = '#00000'.$colorIndex;
        }

        return str_replace($toReplace, $this->colors, $this->svgContent);
    }

    public static function convertSvgToColor($svgFilename, $colors=['#000000'])
    {
        return (new Icon())->loadSvg($svgFilename)->setColors($colors)->processSvg();
    }

    public function renderIcon($svgFilename, $colors)
    {
        $svgLocation = explode('-', $svgFilename);

        if(isset($svgLocation[0]) and !empty($svgLocation[0])) {
            $folderName = $svgLocation[0];
        } else {
            throw new \Exception('"'.$svgFilename.'" is not in correct format (christmas-1)');
        }

        if(isset($svgLocation[1]) and !empty($svgLocation[1])) {
            $svgNumber = $svgLocation[1];
        } else {
            throw new \Exception('"'.$svgFilename.'" is not in correct format (christmas-1)');
        }

        if(substr($svgNumber, -4)!='.svg') {
            $svgNumber.='.svg';
        }

        $svgFileLocation = [
            'folder' => $folderName,
            'svg' => $svgNumber
        ];

        $colors = explode('-', $colors);

        $requestUri = request()->getRequestUri();


        $svg = \SvgIcons\Icon::convertSvgToColor($svgFileLocation, $colors);


        $publicSvgFolder = str_replace('.svg', '', $svgFilename);

        if(!file_exists(public_path('/static/icons'))){
            mkdir(public_path('/static/icons'));
        }
        if(!file_exists(public_path('/static/icons/'.$publicSvgFolder))) {
            mkdir(public_path('/static/icons/'.$publicSvgFolder));
        }

        file_put_contents(public_path($requestUri), $svg);

        return redirect($requestUri);
    }

    public function svgIconList()
    {

        $iconsFolder = realpath(__DIR__.'/../icons');

        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $context = \Cache::get($cacheKey);
        if(is_null($context)) {

            $icons = [];
            foreach (glob($iconsFolder . '/*' , GLOB_ONLYDIR) as $dir){
                $directoryName = substr($dir, strrpos($dir, '/') + 1);
                foreach (glob($dir.'/*.svg') as $svg) {
                    $svgName = substr($svg, strrpos($svg, '/') + 1);
                    $svgName = str_replace('.svg', '', $svgName);
                    $icons[$directoryName][] = $directoryName.'-'.$svgName;
                    sort($icons[$directoryName]);
                }
            }

            $context['icons'] = $icons;


            \Cache::put($cacheKey, $context, \Carbon\Carbon::now()->addHours(24));
        }

        $context['meta']['title'] = 'Icons';

        return view('SvgIcons::icons', $context);
    }

}